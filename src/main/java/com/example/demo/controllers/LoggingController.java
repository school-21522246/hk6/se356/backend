package com.example.demo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/logging")
public class LoggingController {

	private final Logger logger = LoggerFactory.getLogger(LoggingController.class);

	@DeleteMapping("/LogError")
	public void LogError() throws RuntimeException {
		throw new RuntimeException("Error");
	}

	@PutMapping("/LogWarn")
	public void LogWarn() { logger.warn("Warn"); }

	@GetMapping("/LogInfo")
	public void LogInfo() { logger.info("Info"); }
}
