FROM openjdk:23-ea-17-jdk-bullseye as build
WORKDIR /build
COPY mvnw pom.xml  ./
COPY .mvn/ ./.mvn
RUN sh ./mvnw dependency:go-offline
COPY . .
RUN sh ./mvnw clean package -DskipTests

FROM openjdk:23-ea-17-slim-bullseye
WORKDIR /app
EXPOSE 8080
COPY --from=build /build/target/**.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]
