package com.example.demo;

import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import org.springframework.context.annotation.Bean;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;

@SpringBootApplication
public class BackendApplication {
    static Logger logger = LoggerFactory.getLogger(BackendApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(BackendApplication.class, args);
		logger.info("Success");
	}
//	@Bean
//    public WebServerFactoryCustomizer<TomcatServletWebServerFactory> containerCustomizer() {
//        return factory -> factory.addConnectorCustomizers(connector -> connector.setMaxPostSize(15 * 1024 * 1024)); // 10 MB
//    }
	

    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI()
            .addSecurityItem(new SecurityRequirement()
                .addList("Bearer Authentication"))
            .components(new Components().addSecuritySchemes
                    ("Bearer Authentication", new SecurityScheme()
                     .type(SecurityScheme.Type.HTTP)
                     .bearerFormat("JWT")
                     .scheme("bearer")));
    }
}
